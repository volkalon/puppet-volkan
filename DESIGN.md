Teach Volkan some Puppet
========================

Reinforce some basics
---------------------

### Development Environment

So far, we have:

-   updated brew
-   installed [iTerm2](https://iterm2.com/downloads.html)! We now have emoji when running brew 😉
-   [automated brew update](https://tam7t.com/keeping-brew-up-to-date/)
-   installed rbenv & ruby-build ([from brew](https://alyssa.is/ruby-osx/))
-   rbenv installed a ruby
-   set that ruby, globally
-   gem install pry-doc pry-byebug pdk
-   brew install [neovim](https://neovim.io/) & [hub](https://hub.github.com/)

Now, our Development Environment is ready to be configured

### Clean Code

### RSpec-puppet Tests

### Git

Focus on building Infra
-----------------------

### Profiles & Roles

### RSpec-server (integration) Tests

### Patch Upstream Modules

### Custom Facts

### Hiera

### Functions

### Iteration

Building our own Modules
------------------------
